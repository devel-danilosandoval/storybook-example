const Badge = ({ color = 'gray', label ="" }) => {
    return (
        <span className={`px-4 py-2  text-base rounded-full text-white  bg-${color}-500`}>
            {label}
        </span>
    )
}
    

export default Badge