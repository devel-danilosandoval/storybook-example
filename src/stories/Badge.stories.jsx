import React from 'react';

import Badge from '../components/Badge';

export default {
  title: 'Example/Badge',
  component: Badge,
  argTypes: {
    color: { control: 'color' },
  },
};

const Template = (args) => <Badge {...args} />;

export const Default = Template.bind({});
Default.args = {
  label: 'Badge',
};

export const Colorized = Template.bind({});
Colorized.args = {
  label: 'Badge',
  color: "red"
};
